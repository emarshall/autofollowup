# How to use - set up templates.py with your copy
# create a 'followup' label for emails you want fololowing up
# create your credentials
# run the script

import httplib2
import os
import json

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools


from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import base64
import string
from apiclient import errors

import templates as t
# here we set up the export templates for emails that are not worth following up
output_list=['teststring']
output = raw_input("Would you like an exported file of the deals that need to be removed from your CRM?: y/n?")
if output == "y":
  # open a file for printing the disqalified leads to
  targetfile = open('CRM_LOST_DATA.json', 'w')
  targetfile.write('[')
elif output =="n":
  pass
else: exit("We have exited. Please choose either y or n to continue with the tool")


user_from_name = raw_input("Please type your name as you would like it to appear in the from field: ")
print "Your name will be seen as: " + user_from_name
if raw_input("Is this correct y/n?") != "y":
  exit("I have exited for you. Please start again...")
else: pass

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None
invalid_emails = []
SCOPES = 'https://www.googleapis.com/auth/gmail.modify'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'autoFollowUp Script'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'gmail-quickstart.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatability with Python 2.6
            credentials = tools.run(flow, store)
        print 'Storing credentials to ' + credential_path
    return credentials


def CreateMessage(sender, to, email_obj, thread_subject, thread_id=None):
  """Create a message for an email.

  Args:
    sender: Email address of the sender.
    to: Email address of the receiver.
    mesage_object the object containing the subject and message parts

  Returns:
    An object containing a base64url encoded email object.
  """
  # Construct email
  message = MIMEMultipart('alternative')
  message['To'] = to
  message['From'] = user_from_name + " <" + sender['emailAddress'] +">"
  message['Subject'] = thread_subject
 
  # Record the MIME types of both parts - text/plain and text/html.
  part1 = MIMEText(email_obj['textBody'] + user_from_name, 'plain')
  part2 = MIMEText(email_obj['htmlBody'] + user_from_name + email_obj['emailend'], 'html')
 
  # Attach parts into message container.
  # According to RFC 2046, the last part of a multipart message, in this case
  # the HTML message, is best and preferred.
  message.attach(part1)
  message.attach(part2)

  # Now if there is a thread insert it into the thread (most likley)
  # if not we will just send it as a normal message on it's own
  if thread_id == None:
    message['Subject'] = email_obj['subject']
    return {'raw': base64.urlsafe_b64encode(message.as_string())}
  else:
    return {'raw': base64.urlsafe_b64encode(message.as_string()), 'threadId': thread_id}


def CreateDraft(service, user_id, message_body):
  """Create and insert a draft email. Print the returned draft's message and id.

  Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    message_body: The body of the email message, including headers.

  Returns:
    Draft object, including draft id and message meta data.
  """
  try:
    message = {'message': message_body}
    draft = service.users().drafts().create(userId=user_id, body=message).execute()
    # print 'Draft id: %s\nDraft message: %s' % (draft['id'], draft['message'])
    return draft
  except errors.HttpError, error:
    print 'An error occurred: %s' % error
    raise error


def SendDraft(service, user_id, draft_id):
  """Sends the selected draft returns true or error

  Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    draft_id: The id of the draft you wish to send.

  Returns:
    None
  """
  try:
    draft_id = {'id': draft_id}
    service.users().drafts().send(userId=user_id, body=draft_id).execute()
    print "--> Sent the email to the recipient"
  except errors.HttpError, error:
    print 'An error occurred: %s' % error


def ListThreadsMatchingQuery(service, user_id, query=''):
  """List all Threads of the user's mailbox matching the query.

  Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    query: String used to filter messages returned.
           Eg.- 'label:UNREAD' for unread messages only.

  Returns:
    List of threads that match the criteria of the query. Note that the returned
    list contains Thread IDs, you must use get with the appropriate
    ID to get the details for a Thread.
  """
  try:
    response = service.users().threads().list(userId=user_id, q=query).execute()
    threads = []
    if 'threads' in response:
      threads.extend(response['threads'])

    while 'nextPageToken' in response:
      page_token = response['nextPageToken']
      response = service.users().threads().list(userId=user_id, q=query,
                                        pageToken=page_token).execute()
      threads.extend(response['threads'])

    return threads
  except errors.HttpError, error:
    print 'An error occurred: %s' % error


def ListThreadsWithLabels(service, user_id, label_ids=[]):
  """List all Threads of the user's mailbox with label_ids applied.

  Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    label_ids: Only return Threads with these labelIds applied.

  Returns:
    List of threads that match the criteria of the query. Note that the returned
    list contains Thread IDs, you must use get with the appropriate
    ID to get the details for a Thread.
  """
  try:
    response = service.users().threads().list(userId=user_id,
                                              labelIds=label_ids).execute()
    threads = []
    if 'threads' in response:
      threads.extend(response['threads'])

    while 'nextPageToken' in response:
      page_token = response['nextPageToken']
      response = service.users().threads().list(userId=user_id,
                                                labelIds=label_ids,
                                                pageToken=page_token).execute()
      threads.extend(response['threads'])

    return threads
  except errors.HttpError, error:
    print 'An error occurred: %s' % error


def GetThread(service, user_id, thread_id):
  """Get a Thread.

  Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    thread_id: The ID of the Thread required.

  Returns:
    Thread with matching ID.
  """
  try:
    thread = service.users().threads().get(userId=user_id, id=thread_id, format='metadata', metadataHeaders=['To','Subject']).execute()
    messages = thread['messages']
    print ('thread id: %s - number of messages '
           'in this thread: %d') % (thread['id'], len(messages))
    return thread
  except errors.HttpError, error:
    print 'An error occurred: %s' % error


def ModifyThread(service, user_id, thread_id, msg_labels):
  """Add labels to a Thread.

  Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    thread_id: The id of the thread to be modified.
    msg_labels: The change in labels.

  Returns:
    Thread with modified Labels.
  """
  try:
    thread = service.users().threads().modify(userId=user_id, id=thread_id,
                                              body=msg_labels).execute()

    thread_id = thread['id']
    label_ids = thread['messages'][0]['labelIds']

    # print 'Thread ID: %s - With Label IDs %s' % (thread_id, label_ids)
    return thread
  except errors.HttpError, error:
    print 'An error occurred: %s' % error


def CreateMsgLabels():
  """Create object to update labels.

  Returns:
    A label update object.
  """
  return {'removeLabelIds': [], 'addLabelIds': ['UNREAD', 'Label_2']}


def RestructureRepliesFrom(repliesFrom):
  """Takes a list of emails and strips all the crap out
  returns a list of pure emails
  """
  newList = []
  for fromItem in repliesFrom:
    if '<' in fromItem:
      stripped = fromItem[fromItem.index('<')+1:fromItem.index('>')]
    else:
      stripped = fromItem
    newList.append(stripped)
  return newList


def flagReply(service, user_id, thread_id):
  """Get a Thread that has been replied to.

  Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    thread_id: The ID of the Thread required.

  Returns:
    Threads that a customer has replied to in some form.
  """
  try:
    repliesFrom = []
    thread = service.users().threads().get(userId=user_id, id=thread_id, format='metadata', metadataHeaders='From').execute()
    for message in thread['messages']:
      repliesFrom.append(message['payload']['headers'][0]['value'])
    # remove the extra strings so email addresses in <email@site.com> and email@site.com formats are equal
    repliesFrom = RestructureRepliesFrom(repliesFrom)
    # remove duplicates and test for reply from other email address
    if len(list(set(repliesFrom))) > 1:
        # they have replied so return the thread ID so we can de-tag it.
        print "We have flagged a reply in %s" % thread['id']
        return thread['id']
  except errors.HttpError, error:
    print 'An error occurred: %s' % error


def GetUserEmail(service, user_id):
  """Get the logged in users email address

  Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"

  Returns:
    the logged in user's email address
  """
  try:
    return service.users().getProfile(userId=user_id).execute()
  except errors.HttpError, error:
    print 'An error occurred: %s' % error


def markDeadThread(email):
  """Add email to a list that is saved as a file

    Args:
      email: The email of the person we have been sending to! 
  """
  try:
    email = email[email.index('<')+1:email.index('>')]
  except Exception, e:
    pass
  output_list.append(email)
  if output == 'y':
    jsonstring = '"'+ email +'"' + ',' + " "
    print jsonstring
    targetfile.write(jsonstring)
    print "written to file"




def EnsureIs(header_obj, search_term):
  """Takes a header and looks through it's index's 
  for the correct part so we ensure there is no error

  Args:
    thread_object: the thing that needs to be searched

  Returns: The value of the thing with name = search term
  """
  for header in header_obj:
    if header['name'] == search_term:
      return header['value']
    else:
      pass

def IsAnEmail(email):
  """This fuction stops the crashing when someone has set a non ascii charecter as their contact name

  Args:
    email: a string that we are about to send an email to

  Returns:
    True: if has the charecters we expect in
    False: if has a single bad charecter
  """
  chars = string.ascii_letters + string.digits + string.whitespace +"#-_~!$&'()*+,;=:.(),:;<>@[\]"
  if all((c in chars) for c in email):
    return True
  else:
    invalid_emails.append(email)
    return False




def main():
    """Shows basic usage of the Gmail API.

    Creates a Gmail API service object and outputs a list of label names
    of the user's Gmail account.
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)

    # get labels
    results = service.users().labels().list(userId='me').execute()
    labels = results.get('labels', [])
    # =========================================================================
                    # Get a list of labels and check follow up is used
    # =========================================================================
    # labels are selectedd through ID but we want human readable ones from name so construct a dictionay
    if not labels:
        print 'No labels found.'
    else:
      for label in labels:
        if label['name'] == 'followup': 
            followUpLabelId = label['id']
            # print "FOLLOWUP label ID has been found: " + followUpLabelId
        else: 
            "ERROR: No followup Label ID found! Please tag your followups with 'followup'"
    # =========================================================================
            # remove all replied to emails from the follow up label
    # =========================================================================
    # now we will get a list of threads that correspond to the filter (currently live)
    followUpThreads = ListThreadsMatchingQuery(service, 'me', 'label:FOLLOWUP')
    # for each of these threads let's work out whether we have had a reply yet from a customer
    for thread in followUpThreads:
      flaggedThread = flagReply(service, 'me', thread['id'])
      if flaggedThread != None:
          ModifyThread(service, 'me', flaggedThread ,{"removeLabelIds": [followUpLabelId] })
          print "--> Removed label ID from thread: " + followUpLabelId 
      else:
        print "Not removed label as no relevent reply has been found"
    # =========================================================================
      # Get all the threads we want to follow up and send their next template
    # =========================================================================
    # Now lets re-get the list to see what we are working with
    liveThreads = ListThreadsMatchingQuery(service, 'me', 'label:FOLLOWUP')
    # for each thread decide which reply to send and then send it

    for liveThread in liveThreads:
      thread = GetThread(service, 'me', liveThread['id'])
      sendTo = EnsureIs(thread['messages'][0]['payload']['headers'], 'To').replace("'","")
      subject = EnsureIs(thread['messages'][0]['payload']['headers'], 'Subject')
      if IsAnEmail(sendTo) == True:
        if len(thread['messages']) == 1:
          # send first follow up
          MESSAGE_TO_SEND = CreateMessage(GetUserEmail(service,'me'), sendTo, t.email1, subject, thread['id'])
          draft = CreateDraft(service, 'me', MESSAGE_TO_SEND)
          SendDraft(service, 'me', draft['id'])
        elif len(thread['messages']) == 2:
          # send second follow up email
          MESSAGE_TO_SEND = CreateMessage(GetUserEmail(service,'me'), sendTo, t.email2, subject, thread['id'])
          draft = CreateDraft(service, 'me', MESSAGE_TO_SEND)
          SendDraft(service, 'me', draft['id'])
        elif len(thread["messages"]) == 3:
          # send third follow up email
          MESSAGE_TO_SEND = CreateMessage(GetUserEmail(service,'me'), sendTo, t.email3, subject, thread['id'])
          draft = CreateDraft(service, 'me', MESSAGE_TO_SEND)
          SendDraft(service, 'me', draft['id'])
        elif len(thread["messages"]) == 4:
          # send fourth follow up email
          MESSAGE_TO_SEND = CreateMessage(GetUserEmail(service,'me'), sendTo, t.email4, subject, thread['id'])
          draft = CreateDraft(service, 'me', MESSAGE_TO_SEND)
          SendDraft(service, 'me', draft['id'])
        else:
          # remove the tag as we have already sent four follow ups 
          ModifyThread(service, 'me', thread['id'], {"removeLabelIds": [followUpLabelId] })
          print "--> We have now followed up four times - time for a bit of post!"
          print "--> I have added the email to a list for you..."
          markDeadThread(sendTo)
      else:
        pass


    print "the program has finished successfully "
    print "Here is a list of Invalid Email Addresses that we could not send to: "
    print "______________________________________________________________________"
    print "__________________________**INVALID LIST**____________________________"
    print invalid_emails
    print "______________________________________________________________________"
    print "______________________________________________________________________"
    # here we either print a list or save the list as a file so that we can use it elsewhere
    if output == 'y':
      targetfile.write('"script successfully ended" ]')
      targetfile.close()
    elif output =='n': 
      print "                                                                     "
      print "Here is a list of Email Addresses that you should mark as lost in your CRM:"
      print "______________________________________________________________________"
      print "_______________________**TO BE MARKED LOST**__________________________"
      print output_list
      print "______________________________________________________________________"
      print "______________________________________________________________________"
    else: exit("A invalid save choice was made. Program Exited!")




if __name__ == '__main__':
    main()