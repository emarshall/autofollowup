#!/bin/bash
# If you do not have homebrew set up run this script
if ! type "$brew" > /dev/null; then
    echo "brew does not exist"
    sudo easy_install pip 
    sudo pip install virtualenv
fi
# if you have homebrew
# pass



# Whatever we must do this
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt