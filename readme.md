*This is a script that automates follow ups in gmail. 
If no replies are received from the recipient then this script will ping them to get them to reply*

**WHO IS THIS FOR?**

Sales people who send mail merges from their CRM. 

**WHY DID I BUILD IT?**

70% of all leads fail to reply to the first email. We needed a way to remind them to reply without spending hours emailing them ourselves.

**FEATURES**


Integrates with gmail so works with your CRM's mail merge just tag all the emails you sent last week with a "followup" tag

![Screen Shot 2015-07-09 at 12.10.57.png](https://bitbucket.org/repo/xxyj4e/images/1518303787-Screen%20Shot%202015-07-09%20at%2012.10.57.png)


Simple terminal interface. Run the script, enter your name, get your follow ups sent. 

![Screen Shot 2015-07-09 at 12.17.05.png](https://bitbucket.org/repo/xxyj4e/images/3651864034-Screen%20Shot%202015-07-09%20at%2012.17.05.png)


Get friendly reminders sent to all your mail merge contacts so they are more likely to reply.

![Screen Shot 2015-07-09 at 12.22.44.png](https://bitbucket.org/repo/xxyj4e/images/2518760049-Screen%20Shot%202015-07-09%20at%2012.22.44.png)

****

**REQUIREMENTS: Python 2.7 and pip**

**SETUP**

1) Get the code in this git repo

2) Update the templates.py file with your own copy (text and HTML)

3) If you do not have a client_secret.json: Go to https://developers.google.com/gmail/api/quickstart/python and follow instructions to enable the gmail api

4) run install.sh to get all the dependancies set up

5) Create the "followup" tag in your gmail account

6) Run the run.sh script in your terminal

7) Sit back and relax



**GENERAL USAGE**

*Do these steps every Tuesday at around 2pm*

1) Tag all emails from last week that you have not tagged before (in your sent folder) that you want followed up with the tag "followup"

2) Run this script using **sh run.sh**

3) Enter your human readable name as you want it to appear in the "from" field i.e. Ewan from EmailCo

4) Wait for the script to run

5) Send a mail merge out from your CRM

6) Tag all these newly sent emails with the "followup" tag for next week

7) Look at your starred emails. Any people that have been followed up 4 times have been starred and can now have a piece of direct mail sent to them. Or you can just give up.