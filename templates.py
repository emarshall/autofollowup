# These are the emails that will be send as follow ups. We instalise a map for each template so it is simple to change things

email1 = {
    'subject': 'Just to check',
    'textBody': 'Hi,\nI just wanted to check that my previous email had not got lost in your inbox. I would love to hear from you. \n\nMany thanks, \n\n',
    'htmlBody': """\
    <html>
    <head></head>
    <body>
        <p>Hi,</p>
        <p>I just wanted to check that my previous email had not got lost in your inbox. I would love to hear from you.</p>
        <p>Many thanks,</p>
        <p>""",
    'emailend': ' </p> </body> </html> '
}

email2 = {
    'subject': 'A quick chat',
    'textBody': 'I thought you might be interested in this BBC radio extract on how John\'s, one of my favourite SpeakSet users, life has  been improved by SpeakSet.\n\nhttps://soundcloud.com/speakset/bbc-radio-stoke-john-maynard-speakset-user-abridged \n\nI am sure we can help many of your service users and I would love to get your thoughts. \n\nDo you have ten minutes for a brief phone call next week?\n\nBest wishes, \n\n',
    'htmlBody': """\
    <html>
    <head></head>
    <body>
        <p>I thought you might be interested in this BBC radio extract on how John's, one of my favourite SpeakSet users, life has  been improved since getting a device.</p>
        <p><a href="https://soundcloud.com/speakset/bbc-radio-stoke-john-maynard-speakset-user-abridged">Click here to listen to John</a></p>
        <p>I am sure we can help many of your service users and I would love to get your thoughts.</p>
        <p>Do you have ten minutes for a brief phone call next week?</p>
        <p>Best wishes,</p>
        <p>""",
    'emailend': ' </p> </body> </html> '
}

email3 = {
    'subject': 'Quick question',
    'textBody': 'I am writing to follow up my email as I have not heard back from anyone on the team. If it makes sense to talk, let me know how your calendar looks. \n\nIf not please do let me know who the appropriate person would be to talk to.\n\nI also thought you might be interested in some specific cases on how to improve access to care: http://blog.speakset.com/remote-care-in-rural-areas/\n\nBest wishes, \n\n',
    'htmlBody': """\
    <html>
    <head></head>
    <body>
        <p>I am writing to follow up my email as I have not heard back from anyone on the team. If it makes sense to talk, let me know how your calendar looks.</p>
        <p>If not please do let me know who the appropriate person would be to talk to.</p>
        <p>I also thought you might be interested in some specific cases on how to improve access to care: <a href="http://blog.speakset.com/remote-care-in-rural-areas/">Click Here</a></p>
        <p>Best wishes,</p>
        <p>""",
    'emailend': ' </p> </body> </html> '
}

email4 = {
    'subject': 'Just checking',
    'textBody': 'I\'ve reached out to you a few times, to see if we could chat about why SpeakSet would be great for your older service users. I realise that you must be getting hundreds of emails each day, so I\'m going to do you a favour and stop filling up your inbox.\n\nBut I\'d like to ask a favour of you too. I am looking to speak with whomever is responsible for delivering better care more efficiently. If you are that person, could we schedule a time to discuss SpeakSet, and see if it might be a fit for your team?\n\nIf you aren\'t involved in improving care, would you feel comfortable referring me to the right person?\n\nMany thanks, \n\n',
    'htmlBody': """\
    <html>
    <head></head>
    <body>
        <p>I've reached out to you a few times, to see if we could chat about why SpeakSet would be great for your older service users. I realise that you must be getting hundreds of emails each day, so I'm going to do you a favour and stop filling up your inbox.</p>
        <p>But I'd like to ask a favour of you too. I am looking to speak with whomever is responsible for delivering better care more efficiently. If you are that person, could we schedule a time to discuss SpeakSet, and see if it might be a fit for your team?</p>
        <p>If you aren't involved in improving care, would you feel comfortable referring me to the right person?</p>
        <p>Many thanks,</p>
        <p>""",
    'emailend': ' </p> </body> </html> '
}